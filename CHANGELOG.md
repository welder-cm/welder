# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

Lots and lots of changes and improvements over the projects predecessor. Most of them are covered here.

### Added

- general: provide a Dockerfile
- general: new sub-command: graph
- general: new sub-command: exec-local
- config: add playbook specific variables (borrowed by a fork of welder: github.com/homelinen/welder)

### Changed

- general: replace Ruby by Python and thus liquid by Jinja2 (the templates remain mostly compatible)
- general: only execute one specific script in each module: `setup.sh` (otherwise scripts in the files subfolder got executed too)
- general: safe cleanup also on interruption
- general: opt in for `sudo` password query (`ask_sudo_password: true` in `<playbook>.yml`)
- general: `expect` is not required anymore as it's built in now
- config: flatten config file key hierarchies
- config: variable injection per default into remote script
- config: transform all config variables to uppercase to follow shell script conventions (env variables shall be uppercase)
- config: swap `$cfg_` prefix by `$W_` for config variables
- config: inject files root path into remote script: `$W`
- config: introduce shell variable escaping for the config values
- vault: replace plain text `vault.yml` by a lookup mechanism via `gopass`
- vault: make secret values available in template file variable substitution

### Fixed

- general: no more unsafe `sudo` password passing to another process via a CLI parameter: It's handled internally now

