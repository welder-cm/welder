FROM python:3.6-slim-buster
# Build: docker build -t tmcw/welder:latest .
# Usage: docker run -ti --rm -v /path/to/project:/project tmcw/welder welder run playbook.yml

RUN \
    apt-get update -qq \
    && apt-get install -qq --yes --no-install-recommends rsync openssh-client gopass \
    && mkdir /welder

# slow dependency installation first, to speed up image recreation on code update
COPY setup.py /welder/
RUN pip install /welder

# install welder properly
COPY . /welder
RUN pip install /welder

VOLUME /project
WORKDIR /project

