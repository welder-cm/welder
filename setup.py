from setuptools import setup, find_packages
import os
from pathlib import Path

share = Path(sys.prefix, 'share')
#if 'XDG_DATA_HOME' in os.environ:
#    share = Path(os.environ['XDG_DATA_HOME'])
#else:
#    share = Path.home() / '.local' / 'share'

setup(
    name="welder",
    version="2.0-RC1",
    description="Micro configration management using shell scripts",
    author=['Thomas McWork'],
    author_email='thomas.mc.work@gmail.com',
    url='https://gitlab.com/welder-cm/welder',
    python_requires='>=3',
    install_requires=[
        'ruamel.yaml',
        'jinja2',
        'click',
        'pexpect',
        'graphviz',
    ],
    license='AGPL3',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'welder=welder.cli:cli_entry'
        ]
    },
    zip_safe=True
)
