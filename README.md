# 👨‍🏭 Welder

__<div align="center">[Features](#features) · [Setup](#setup) · [Configuration](#configuration) · [Usage](#usage) · [Best practices](#best-practices) · [Comparison](#comparison) · [Project roots](#project-roots)</div>__
 
Welder allows you to set up a Linux server with plain shell scripts.

![demonstrating procedure in a shell](docs/demo.svg)

It has been created as a lightweight replacement for Ansible. Ansible is great peace of software, but for many tasks it's just overkill. More often than not I need is:

``` sh
ssh -t user@example.com "$(cat ./my-setup-script.sh)"
```

In most basic terms, that's what welder does. And some more …

### Origin

This project is a semi-official derivate of [the original welder project]( https://github.com/pch/welder)

[More about this, a list of changes and migration notes can be found further below.](#project-roots)

## Features

Welder allows you to:

- execute plain shell scripts on the server
- organize your scripts into a logical set of reusable modules
- set up a server with a single command (`welder run <playbook>`)
- run one-off shell scripts (`welder exec <user@example.com> <path/to/script.sh>`)
- (optional) usage of [Jinja](https://jinja.palletsprojects.com/) templates
- (optional) enter `sudo` password just once per playbook
- create a visual representation of your setup structure (using the DOT language of graphviz)

## Setup

### Requirements

#### Local

- Python 3
- `rsync`
- SSH-Client (`ssh` from `openssh-client`)

#### Remote

- SSH-Server (`sshd` from `openssh-server`)
- Whatever language your `setup.sh` is written in. Although the file extension is suggesting a shell script, it can be virtually anything. Even a binary file.

### Installation

    pip install git+https://gitlab.com/welder-cm/welder

#### Shell completion

Command completion for bash is included:

    eval "$(_WELDER_COMPLETE=source_bash welder)"

Intead of using it on demand via `eval` the output could also be written to a file that is sourced by the `.bashrc`.

## Configuration

### Directory structure

An example directory structure:

``` sh
.
├── modules
│   ├── nginx
│   │   ├── files
│   │   │   ├── nginx.conf
│   │   └── setup.sh
│   ├── rails
│   │   ├── files
│   │   │   └── nginx.conf.j2
│   │   └── setup.sh
│   ├── system
│   │   ├── files
│   │   │   ├── 10periodic
│   │   │   └── 50unattended-upgrades
│   │   └── setup.sh
├── config.yml
└── my-site.yml
```

Example playbook:

``` yaml
ssh:
  url: admin@example.com
  port: 2222 # Optional (default: 22)

# List of modules to execute
modules:
  - nginx
  - rails
  - system
```

### File access

All required files are uploaded to the remote machine automatically. See the next section for templates. The remote files path is available in the `setup.sh` via the environment variable `$W`.

### Templates

Welder uses [Jinja](https://jinja.palletsprojects.com/) for templates.

``` lua
# modules/rails/files/nginx-site.conf.j2
upstream thumbor {
  {% for port in thumbor_instances %}
      server 127.0.0.1:{{ port }};
  {% endfor %}
}

server {
    listen 80;
    server_name {{ thumbor_host }};

    location / {
        proxy_pass http://thumbor;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

### config.yml

The `config.yml` file will be used to provide variables across the all playbooks:

``` yaml
# example config.yml
app:
  domain: example.com
  dir: "/var/www/example"

thumbor_host: images.example.com
thumbor_instances:
  - 8000
  - 8001
  - 8002
  - 8003
```

During the compilation phase, `config.yml` is turned into a Bash-compatible format and injected into each execution of the `setup.sh`:

``` sh
# modules/example/setup.sh

# (notice the $W_ prefix)
echo $W_APP_DOMAIN
echo $W_APP_DIR
```

For playbook specific settings variables can be defined in `<playbook>.yml`.

### Shared Modules

If you want to avoid duplicating modules across different projects, you can specify `shared_path` in your playbook's YAML file:

``` yaml
ssh:
  url: admin@example.com

shared_path: ../shared

modules:
  - system
  - firewall
```

In the example above, `../shared` directory should contain a `modules` directory. In case of a duplicate module, the 
local one will be preferred.

### Local preparation

If a module contains a file named `prepare.sh`, then it'll be executed locally with all variables injected. Beyond that there will be three additional environment variables:

- `$WP_SOURCE`: Path of the module, e.g. `./modules/roundcube`. From here you can grab files which shall not be part of the `files` folder
- `$WP_TARGET`: Path to the prepared upload folder with all files of the previous compilation phase. E.g. `./tmp/roundcube/files`
- `$WP_TEMP`: A per module temporary directory for intermediate files which will be disposed after the preparation phase: E.g. `./tmp/roundcube/tmp`


### Secrets

You can safely provide sensitive values like passwords. There is an integration of 
[the semi-default tool `gopass`](https://www.gopass.pw/), which is itself based on 
[the outdated `pass`](https://www.passwordstore.org/) and 
[it's ecosystem](https://www.passwordstore.org/#other).

You can simply prefix any value by `gopass:` followed by the gopath secret path, e.g.:

``` yaml
ldap:
  password:
    admin_username: admin
    admin_password: gopass:github/welder/admin-user
    dev_password: gopass:github/welder/dev-user
```

The values are looked up in the module execution phase.

Don't store any sensitive information in plain text in the `config.yml`, as it's designates for being tracked by a VCS like git. You probably don't want publish them e.g. in a public repo.

### sudo

It's possible to let welder ask for a sudo password beforehand. It can be enabled in one of the config files (`config.yml` or `<playbook>.yml`):

    ask_sudo_password: true

This feature is optional, as it's breaking the unattended nature of welder.

### Example setup script

``` sh
# modules/nginx/setup.sh

apt update && sudo apt install -y nginx

cp "$W/nginx.conf" /etc/nginx/nginx.conf

# Disable default site
if [ -f /etc/nginx/sites-enabled/default ]; then
  rm /etc/nginx/sites-enabled/default
fi

systemctl restart nginx
```

### Lifecycle

The welder lifecycle is similar to the one of Apache maven. It is defined by a list of phases. Each phase can be invoked by a separate sub-command.

1. `config` – collect and merge all config parameters
2. `prepare` – clone the source files, compile the templates and run a custom preparation step
3. `deploy` – upload to the remote machine
4. `run` – run the setup on the remote machine
5. `clean` – clean up both locally and remotely

These phases are executed sequentially to complete the lifecycle. Given the lifecycle phases above, this means that welder will first compose the configuration values, then will prepare the local files, upload those to the remote machine, run `setup.sh` remotely and finally clean up everything.

When executing on of the phases, then every preceding phase will also be executed.

## Usage

**⚠ Warning ⚠**

> Since welder allows you to run **anything** on the server, you should use it with caution. It won't protect you from screw-ups, like
`rm -rf "/$undefined_variable"`. (but in this case you can limit the damage via `set -o nounset`)

---

Run the playbook defined in my-site.yml:

``` sh
welder run my-site
```

The `run` script will for each module in a playbook:

1. compile templates and configs
2. optional: execute `prepare.sh` locally if existing
2. upload prepared files and all static files to the server (to `$HOME/.welder`)
3. execute the `setup.sh` script
4. cleanup the remote files folder

Additional commands:

``` sh
# compiles templates and uploads them to the server
welder compile <playbook>

# remove compiled files from the server
welder cleanup <playbook>

# run a single `*.sh` script on the server, you can use this:
welder run-script <user@example.com> <local/path/to/script.sh>
```

**NOTE**: the `run-script` command does not compile templates. It merely wraps `ssh -t user@example.com "$(< ./path/to/script.sh)"`. If you want access to templates and config, run `welder compile <playbook>` first and `welder cleanup <playbook>` when you're done.

### Local execution

Run a local executable with the injected welder variables:

    welder exec-local <playbook> path/to/script

The path to the script will be relative to the root path of the project if it is a relative path.

### Create a graph

Welder can create a graph representing the structure of your setup. You can choose which playbooks shall be considered for creation:

    welder graph *.yml > structure.dot

The result is in [the DOT language](https://graphviz.org/doc/info/lang.html) of [the graphviz project](https://graphviz.org/). 

You can also show the result on the fly:

    welder graph server1-*.yml | dot -Tpng | display

## Best practices

Basically it's recommended to avoid to store big files like deb packages, large binaries or pictures. It's better to upload them to a (public or private) CDN or provide them via the artifacts of your CI pipeline. Then they can be downloaded conditionally in the `setup.sh`, based on the fact if they already exist or not.

- Download a deb archive and extract files from it on the fly

      libmaxmind_url="http://ftp.de.debian.org/debian/pool/main/libm/libmaxminddb/libmaxminddb0_1.3.2-1_amd64.deb"
      target_path="opt/goaccess"
      
      curl -L "$libmaxmind_url" \
        | dpkg-deb --fsys-tarfile - \
        | tar -xC "$target_path" --strip-components 4 \
          "./usr/lib/x86_64-linux-gnu/libmaxminddb.so.0" \
          "./usr/lib/x86_64-linux-gnu/libmaxminddb.so.0.0.7"

- Download `tar.gz` files silently and extract into a specific folder:

      curl -Ss "https://…/file.tar.gz" | tar -xzC "/target/path" --strip-components 1

- Favor POSIX compliant `sh` resp. `dash` wherever possible over `bash` to achieve better portability and better performance. Also use the env based shebang:

      #!/usr/bin/env sh

- Fail fast on script errors and undefined variables. That prevents you from failing hard later on:

      set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
      set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

### Idempotency

Although harder to achieve in shell script compared to ansible, it's possible to implement idempotent behaviuor:

- soft links: `ln -sf …`
- move files: `mv -f …`
- create folders: `mkdir -p`
- remove files/folders: `rm -f`

## Comparison

|| Welder | [Ansible](https://github.com/ansible/ansible) | [aviary.sh](https://github.com/team-video/aviary.sh) |
|--------|---|---|---|
| instruction style | imperative | declarative | imperative |
| workflow principle | push | push | pull |
| topology | agent-less | agent-less | agent required |
| community size | very small | huge | small |
| complexity | very low | medium | low |
| target audience | beginners / intermediate | intermediate / advanced | beginners / intermediate |
| Notes | simple and easy | higher entry barrier due to the custom DSL | the whole workflow is bound to a remote git repository |

Do you know other competing solutions or aspects that should be added? Then let me know in the issues!

## Project roots

This project is based on the concept of <https://github.com/pch/welder>.

__Thank you very much, [Piotr](https://github.com/pch), for your inspiration and concept!!__

I decided to re-implement this project due to the following reasons:

- it was implemented in Bash scripts, which led to some limitations
- the project isn't active anymore, and the original author isn't interested in a further development
- it uses the MIT license

### Improvements

See [the first release in the CHANGELOG.md](CHANGELOG.md#unreleased)

### Migration

- replace absolute references to the `setup` folder by the new variable `$W`: `grep -nr "setup/"`
- convert all variables in to the new scheme (`$cfg_app_name` => `$W_APP_NAME`): `grep -nr "\$cfg_"`
- config loading lines like these need to be removed:
    - `[[ -f setup/config-variables ]] && source setup/config-variables`
    - `. setup/config-variables`