#!/usr/bin/env sh
# Create container for the SSH Server + Client
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

BASEDIR=$(dirname "$0")

docker image inspect welder-cm/welder:latest >/dev/null 2>&1 \
   || docker build -t welder-cm/welder:latest "${BASEDIR}/.." > /dev/null
