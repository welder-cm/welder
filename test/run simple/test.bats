#!/usr/bin/env bats

load ../test_helper

@test "verify" {
  run welder run playbook1

  [ $status -eq 0 ]
  
  [ ! -f project/tmp/goaccess/files/a.cfg.j2 ]
  [ "$(ssh_ cat a.cfg)" = "value: v2" ]
  [ "$(ssh_ cat b.cfg)" = "value: v3" ]
  [ "$(ssh_ cat c.cfg)" = "33" ]

  # Will be needed for a `welder compile` test case
  #[ -f project/tmp/goaccess/setup.sh ]
  #[ ! -f project/tmp/goaccess/README.md ]
  #[ -f project/tmp/goaccess/files/a.cfg ]
}

