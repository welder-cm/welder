#!/usr/bin/env sh
# Create container for the SSH Server + Client
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

BASEDIR=$(dirname "$0")

docker image inspect welder-cm/sshd:latest > /dev/null \
  || docker build -t welder-cm/sshd:latest "${BASEDIR}/docker/sshd" > /dev/null
