# Wrap the welder exeuction in the container into a handy command
welder() {
    docker run --rm --network welder-net \
      --volume "${BATS_TEST_DIRNAME}/project:/project" \
      --volume "${BATS_TEST_DIRNAME}/../docker/sshd/id_ed25519:/root/.ssh/id_ed25519:ro" \
      --volume "${BATS_TEST_DIRNAME}/../docker/sshd/known_hosts:/root/.ssh/known_hosts:ro" \
      welder-cm/welder welder "$@"
}

# Simplify the remote command execution on the target server
ssh_() {
    docker run --rm --network welder-net \
    --volume "${BATS_TEST_DIRNAME}/../docker/sshd/id_ed25519:/root/.ssh/id_ed25519:ro" \
    welder-cm/sshd ssh sshd "$@"
}
