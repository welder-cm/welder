#!/usr/bin/env sh
# Run the system tests with isolation based on docker.
# Param 1: Path to a test file. Defaults to all test files that can be found
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

BASEDIR=$(dirname "$0")

$BASEDIR/build-image-welder.sh
$BASEDIR/build-image-sshd.sh

file_list=${@:-$BASEDIR/*/*.bats}

sc=0

# create the shared network
docker network inspect welder-net >/dev/null 2>&1 \
  || docker network create welder-net > /dev/null

# allow to carry on when a test fails
set +o errexit

for testfile in "$file_list"; do
    # create remote server instance per test to enable a clean environment
    docker run --rm --network welder-net --name sshd --detach welder-cm/sshd > /dev/null

    # run the tests
    echo "··· ${testfile} ························"
    bats "$testfile"

    sc=$((sc+$?))

    # stop and remove the remote server container
    docker stop sshd > /dev/null
done

# finally remove the network
docker network rm welder-net >/dev/null

set -o errexit

if [ $sc -ne 0 ]; then
    echo
    echo "Some tests have failed!"
fi

exit "$sc"
