import os
import subprocess
import sys
from pathlib import Path
from typing import List, Dict, Optional, Tuple

from ruamel import yaml

from welder import launcher
from welder.config import Config
from welder.cli_helper import print_fail, print_cmd
from welder.local_cache import LocalCache


class Workspace:
    MODULES_FOLDER_NAME = 'modules'
    """Name of the folder containing the modules"""

    TMP_FOLDER = 'tmp'
    """Temporary folder to persist the prepared files before being uploaded"""

    def __init__(self, base_dir: Path, shared_paths: List[Path]):
        self.base_dir = base_dir
        self.shared_folders = [Path(folder) for folder in shared_paths]

    def local_cache(self, clean_up_on_exit: Optional[bool] = True):
        return LocalCache(self.base_dir / self.TMP_FOLDER, clean_up_on_exit)

    def get_module_path(self, module_name: str):
        module_path_inside = self.base_dir / self.MODULES_FOLDER_NAME / module_name

        if module_path_inside.is_dir():
            return module_path_inside

        module_path_shared = self.__get_from_shared_folder(module_name)
        if module_path_shared:
            return module_path_shared

        print_fail(
            'module "{}" not found at the expected locations: {}:'.format(
                module_name, [self.base_dir, *self.shared_folders]))
        sys.exit(3)

    def __get_from_shared_folder(self, module_name: str):
        for shared_folder in self.shared_folders:
            module_path = shared_folder / self.MODULES_FOLDER_NAME / module_name

            if module_path.is_dir():
                return module_path

        return None

    @staticmethod
    def run_executable(cmd: List[str], config: Dict, cwd: Path):
        run_env = dict(launcher.create_shell_vars('W_', config))
        os_env = os.environ.copy()
        new_env = {**os_env, **run_env}

        try:
            subprocess.run(cmd, env=new_env, check=True, cwd=str(cwd))
        except subprocess.CalledProcessError as cpe:
            print_fail('Local script execution failed with status code {}:'.format(cpe.returncode))
            print_cmd(cmd)
            sys.exit(cpe.returncode)

    def load_config(self, playbook_name: str, config_extra: Optional[Tuple[str, str]] = {}) -> Config:
        config = self.load_config_files(playbook_name)

        for extra_key, extra_value in config_extra:
            config.set(extra_key, extra_value)

        config.resolve_secrets()
        return config

    def load_config_files(self, playbook_name: str) -> Config:
        """Load all involved config files (config.yml, $playbook.yml, vault.yml, $playbook.vault.yml)"""
        config = Config()
        config.set('ssh.port', 22)

        # global
        config_file_global = self.base_dir / 'config.yml'
        if config_file_global.is_file():
            values = Workspace.load_yaml_file(config_file_global)
            config.merge(values)

        # playbook
        config_file_playbook = self.base_dir / '{}.yml'.format(playbook_name)
        if config_file_playbook.is_file():
            values = Workspace.load_yaml_file(config_file_playbook)
            config.merge(values)

            if not config.contains_key('ssh.url'):
                print_fail("'ssh.url' variable must be set in {}.yml".format(playbook_name), 2)
        else:
            print_fail('playbook file not found: "{}"'.format(config_file_playbook), 1)

        return config

    @staticmethod
    def load_yaml_file(file: Path) -> Dict:
        try:
            with file.open('r') as stream:
                return yaml.safe_load(stream) or dict()
        except yaml.YAMLError as exc:
            print(exc, file=sys.stderr)
            sys.exit(1)
