import shlex
from typing import Dict, Optional, Any


def create_launcher_script(config: Dict[str, Any], exec_command: str) -> str:
    """
    Create a launcher shell script that provides all config values as exported environment variables and invoke the
    provisioning script.

    :param config: A dict containing the config values. Can bee a deep nested hierarchy
    :param exec_command: Path to the to be executed command
    :return: The composed launcher script
    """
    shell_var_commands = create_shell_var_commands(config)

    return __create_launcher_script(shell_var_commands, exec_command)


def create_shell_var_commands(config):
    shell_vars = create_shell_vars('W_', config)
    shell_vars_quoted = {key: shlex.quote(value) for key, value in shell_vars}
    return "\n".join("export {}={}".format(key, value) for key, value in shell_vars_quoted.items())


def create_shell_vars(prefix: str, config: Dict[str, Any]) -> Dict[str, str]:
    flat_config = __flatten(config)
    for key in flat_config:
        yield __create_shell_variable(prefix, key, flat_config[key])


def __create_launcher_script(lines: str, exec_command: str, shell: Optional[str] = "bash") -> str:
    """
    Bring it all together in a shell script.

    :param lines:
    :param exec_command:
    :param shell:
    :return:

    >>> print(__create_launcher_script("uname\\nhostname", "/usr/bin/ls", "zsh"))
    #!/usr/bin/env zsh
    set -o nounset
    set -o errexit
    uname
    hostname
    /usr/bin/ls
    """
    return "\n".join(["#!/usr/bin/env {}".format(shell),
                      "set -o nounset",
                      "set -o errexit",
                      lines,
                      exec_command
                      ])


def __create_shell_variable(prefix: str, key: str, value: str):
    """
    Create a shell compatible key/value pair for a variable definition. Quoting will not be applied.

    :param key: Variable name
    :param value: Variable value
    :return:

    >>> __create_shell_variable('W_', 'app_name', 'welder')
    ('W_APP_NAME', 'welder')
    """

    new_key = prefix + key.upper() if key != '' else 'W'
    new_value = __convert_value(value)

    return new_key, new_value


def __convert_value(value: Any) -> str:
    """
    Convert python variable value into a shell variable value.

    :param value: python value
    :return: converted value into shell format

    >>> __convert_value(["a", "b", "c"])
    '(a b c)'

    >>> __convert_value(True)
    '1'

    >>> __convert_value(2)
    '2'
    """
    if isinstance(value, list):
        return '({})'.format(' '.join(item for item in value))
    elif isinstance(value, bool):
        return '1' if value else '0'
    else:
        return str(value)


def __flatten(source: Dict[str, Any], parent_key: Optional[str] = '', sep: Optional[str] = '_') -> Dict[str, Any]:
    """
    Flatten all keys into a shallow list.

    >>> __flatten({'a': {'b': 4}})
    {'a_b': 4}
    """

    items = []
    for key, value in source.items():
        new_key = parent_key + sep + key if parent_key else key
        try:
            items.extend(__flatten(value, new_key).items())
        except AttributeError:
            items.append((new_key, value))

    return dict(items)
