import logging
import shutil
from pathlib import Path


class LocalCache:
    """Local directory for storing temporary files. Will be cleaned up on exit per default."""

    def __init__(self, temp_path: Path, cleanup_on_exit: bool):
        self.cleanup_on_exit = cleanup_on_exit
        self.temp_path = temp_path

    def __enter__(self):
        self.__cleanup()
        self.temp_path.mkdir()

        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.cleanup_on_exit:
            self.__cleanup()

    def __cleanup(self):
        logging.debug('local cache: cleanup')
        if self.temp_path.is_dir():
            shutil.rmtree(self.temp_path)

    def get_module_path(self, module_name: str):
        return self.temp_path / module_name
