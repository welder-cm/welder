import os
import shlex
import sys
from enum import Enum
from typing import List, Optional


class Colors(Enum):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    ORANGE = '\033[33m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def print_status(status: str, color: Colors, msg: str, end: str = os.linesep):
    # When you change the end parameter, the buffer no longer gets flushed. To ensure that you get output as soon as
    # print() is called, you also need to use the flush=True parameter.
    print("  [{}{}{}] {} ".format(color, status, Colors.ENDC.value, msg), end=end, flush=True)


def print_info(msg: str):
    print_status(' .. ', Colors.OKBLUE.value, msg, end='')


def print_success(msg: str):
    print_status(' OK ', Colors.OKGREEN.value, msg)


def print_user(msg: str):
    print_status(' ?? ', Colors.ORANGE.value, msg, end='')


def print_warn(msg: str):
    print_status('WARN', Colors.WARNING.value, msg)


def print_fail(msg: str, status_code: Optional[int] = None):
    print_status('FAIL', Colors.FAIL.value, msg)

    if status_code:
        sys.exit(status_code)


def print_indented(msg: str):
    print(' ' * 8 + ' {}'.format(msg))


def print_cmd(cmd: List[str]):
    print_indented('# ' + ' '.join(shlex.quote(part) for part in cmd))
