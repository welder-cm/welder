import os
import stat
import subprocess
import sys
import tempfile
from pathlib import Path
from typing import Dict

import jinja2

from welder import launcher
from welder.cli_helper import print_fail

EXTENSION = '.j2'


class Module:

    def __init__(self, config: Dict, cache_dir: Path, module_path: Path):
        self.config = config
        self.cache_dir = cache_dir
        self.module_path = module_path

    def prepare(self):
        self.__clone_sources()
        self.__compile_templates()
        self.__custom_step()

    def __clone_sources(self):
        sync_cmd = ['rsync', '--archive', '--delete', '--quiet',
                    '--link-dest', str(self.module_path.absolute()),
                    '--exclude=*{}'.format(EXTENSION),  # omit templates
                    '--include', 'files/',  # the files folder
                    '--include', 'files/**',  # the files folder contents
                    '--include', '/setup.sh',  # the provisioning script
                    '--exclude', '*',  # finally: exclude everything else
                    '{}/'.format(self.module_path.absolute()),
                    self.cache_dir]
        try:
            subprocess.run(sync_cmd, check=True)
        except subprocess.CalledProcessError as cpe:
            print('failed to prepare files (status code: {})'.format(cpe.returncode))
            sys.exit(1)

        setup_file = self.cache_dir / 'setup.sh'
        os.chmod(setup_file, setup_file.stat().st_mode | stat.S_IXUSR)

    def __compile_templates(self):
        errors = []

        for template_file in list(self.__collect_template_files()):
            try:
                out_file = self.cache_dir / \
                           template_file.parent.relative_to(self.module_path) / \
                           template_file.name[:-len(EXTENSION)]  # strip off file extension

                jenv = jinja2.Environment(undefined=jinja2.StrictUndefined, loader=jinja2.BaseLoader())
                result = jenv.from_string(template_file.read_text()).render(self.config)
                out_file.write_text(result)
            except jinja2.UndefinedError as ue:
                errors.append('variable {} in file "{}"'.format(ue.message, template_file))

        if errors:
            print()
            for error in errors:
                print_fail(error)
            sys.exit(4)

    def __collect_template_files(self):
        for root, dirs, files in os.walk(self.module_path / 'files'):
            cur_dir = Path(root)
            for filename in files:
                file_path = cur_dir / filename

                if file_path.name.endswith(EXTENSION):
                    yield file_path

    def __custom_step(self):
        executable_path = self.module_path / 'prepare.sh'

        if executable_path.exists():
            with tempfile.TemporaryDirectory(dir=self.cache_dir) as tmp_folder:
                prepare_vars = {
                    'source': self.module_path,  # the source module path: ./modules/<module>
                    'target': self.cache_dir / 'files',  # the target path containing the prepared files: ./tmp/<module>
                    'temp': tmp_folder  # a temp path fot the script: ./tmp/<module>/tmp
                }

                values_config = dict(launcher.create_shell_vars('W_', self.config))
                values_prepare = dict(launcher.create_shell_vars('WP_', prepare_vars))
                env_vars = {**values_config, **values_prepare}

                self.run_executable(executable_path, env_vars)

    @staticmethod
    def run_executable(executable_path: Path, env_vars: Dict):
        os_env = os.environ.copy()
        new_env = {**os_env, **env_vars}

        try:
            subprocess.run(str(executable_path), env=new_env, check=True)
        except subprocess.CalledProcessError as cpe:
            print('failed to execute "{}" script (status code: {})'.format(executable_path, cpe.returncode))
            sys.exit(1)
