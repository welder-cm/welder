import logging
import shlex
import subprocess
import sys
from pathlib import Path
from typing import Dict, List, Optional

from welder import launcher
from welder.cli_helper import print_fail, print_cmd, print_indented
from welder.ssh_connection import SshConnection


class RemoteServer:
    REMOTE_TEMP_PATH = Path('.welder')
    """"Name of the remote folder to hold the temporary uploaded data."""

    def __init__(self, ssh: SshConnection):
        self.ssh = ssh

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__cleanup()

    def upload(self, source_path: Path):
        try:
            self.ssh.upload(source_path, self.REMOTE_TEMP_PATH)
        except subprocess.CalledProcessError as cpe:
            print_fail('file upload failed with status code {}:'.format(cpe.returncode))
            print_cmd(cpe.cmd)
            sys.exit(2)

    def execute_cmd(self, config: Dict, exec_cmd: Path, cli_args: Optional[List[str]] = []):
        exec_cmd_list = [str(self.REMOTE_TEMP_PATH / str(exec_cmd))] + cli_args
        exec_cmd = ' '.join(shlex.quote(part) for part in exec_cmd_list)
        extra_vars = {'': self.REMOTE_TEMP_PATH / 'files'}
        script_config = {**config, **extra_vars}
        launcher_script = launcher.create_launcher_script(script_config, exec_cmd)

        try:
            self.ssh.exec_script(launcher_script, True)
        except subprocess.CalledProcessError as cpe:
            print()
            print_fail('setup script execution failed with status code {}:'.format(cpe.returncode))
            sys.exit(2)

    def __cleanup(self):
        """Remove all created files on the server."""
        logging.debug('remote server: cleanup')

        cmd = ['rm', '--force', '--recursive', str(self.REMOTE_TEMP_PATH)]

        try:
            self.ssh.exec(cmd)
        except subprocess.CalledProcessError as cpe:
            print_fail('clean up of remote files failed with status code {}:'.format(cpe.returncode))
            print_indented('manual clean up command: ')
            print_cmd(cmd)
            sys.exit(3)
