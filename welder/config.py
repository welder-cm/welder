import subprocess
import sys
from functools import reduce
from typing import Dict, Any, List, Optional

from welder.cli_helper import print_fail, print_cmd


class Config:

    def __init__(self, content: Optional[Dict] = None):
        self.content = content or {}

    def set(self, key_path: str, value: Any):
        Config.set_func(self.content, key_path, value)

    @staticmethod
    def set_func(config_obj: Dict, key_path: str, value: Any):
        if "." in key_path:
            key_parts = key_path.split(".", 2)
            key_left = key_parts[0]
            key_right = key_parts[1]

            if key_left not in config_obj:
                config_obj[key_left] = {}

            sub_conf = config_obj.get(key_left)
            Config.set_func(sub_conf, key_right, value)
        else:
            config_obj[key_path] = value

    def get(self, key_path, default=None):
        if not key_path:
            return default

        return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, key_path.split("."),
                      self.content)

    def contains_key(self, key: str) -> bool:
        """
        Return whether the key exists in the config.

        :param key:
        :return:
        >>> Config({'x': {'y': 3}}).contains_key("x.y")
        True
        >>> Config({'x': {'z': 5}}).contains_key("x.y")
        False
        """
        try:
            reduce(lambda d, sub_key: d[sub_key], key.split("."), self.content)
            return True
        except KeyError:
            return False

    def merge(self, another: Dict, path: List[str] = None):
        Config.merge_func(self.content, another, path)

    @staticmethod
    def merge_func(source: Dict, another: Dict, path: List[str] = None):
        """Merge all values into current config"""

        path = path or []
        for key in another:
            if key in source:
                if isinstance(source[key], dict) and isinstance(another[key], dict):
                    Config.merge_func(source[key], another[key], path + [str(key)])
                else:
                    source[key] = another[key]
            else:
                source[key] = another[key]

    def resolve_secrets(self):
        self.content = SecretSolver.resolve(self.content)


class SecretSolver:
    GOPASS_PREFIX = 'gopass:'

    @staticmethod
    def resolve(content: Dict) -> Dict:
        """Resolve all value prefixed by GOPASS_PREFIX."""
        result = {}

        for key in content:
            value = content[key]

            if isinstance(value, dict):
                result[key] = SecretSolver.resolve(value)
            elif isinstance(value, str) and value.startswith(SecretSolver.GOPASS_PREFIX):
                secret_path = value[len(SecretSolver.GOPASS_PREFIX):]
                result[key] = SecretSolver.__resolve_secret(secret_path)
            else:
                result[key] = value

        return result

    @staticmethod
    def __resolve_secret(secret_path: str) -> str:
        gopass_cmd = ['gopass', 'show', '--password', secret_path]
        outcome = subprocess.run(gopass_cmd, stdout=subprocess.PIPE)

        if outcome.returncode == 0:
            return outcome.stdout.decode('utf-8')
        else:
            print_fail('Failed to resolve secret: {}'.format(secret_path))
            print_cmd(gopass_cmd)
            sys.exit(2)
