import logging
import shutil
from pathlib import Path
from typing import List, Tuple, Optional, Dict

import click
from click import Context

from welder.cli_helper import print_fail
from welder.cmds.exec_remote_cmd import ExecRemoteCmd
from welder.cmds.prepare_cmd import PrepareCmd
from welder.cmds.config_cmd import ConfigCmd
from welder.cmds.exec_local_cmd import ExecLocalCmd
from welder.cmds.graph_cmd import GraphCmd
from welder.cmds.run_cmd import RunCmd
from welder.workspace import Workspace


class KeyValueParamType(click.ParamType):
    name = "kv-param"

    def convert(self, value: str, param, ctx):
        if '=' in value:
            components = value.split('=', 2)
            return components[0], components[1]
        else:
            self.fail(
                "value must consist of a key-value pair separated by a '=' character, got "
                f"{value!r} of type {type(value).__name__}",
                param,
                ctx,
            )


@click.group()
@click.option('--verbose', '-v', is_flag=True, type=bool, default=False)
@click.option('--path', '-p', type=Path)
@click.option('--shared-path', '-s', type=Path, multiple=True,
              help='Additional workspace path containing modules to be used')
@click.pass_context
def cli(ctx: Context, verbose: bool, path: Path, shared_path: List[Path] = []):
    """
    Welder – micro configuration managment using shell scripts
    """

    if not shutil.which('rsync'):
        print_fail('"rsync" is required to run welder', 1)

    if verbose:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)7s %(message)s')

    ctx.obj = Workspace(path if path else Path(), shared_path)


@cli.command("config")
@click.option('--param', '-p', multiple=True, type=KeyValueParamType())
@click.argument('playbook', required=True)
@click.pass_obj
def config_print(workspace: Workspace, param: Optional[Tuple[str, str]], playbook: str):
    config = workspace.load_config(playbook, param)
    ConfigCmd().run(config)


@cli.command()
@click.option('--param', '-p', multiple=True, type=KeyValueParamType())
@click.argument('playbook', required=True)
@click.pass_obj
def prepare(workspace: Workspace, param: Optional[Tuple[str, str]], playbook: str):
    config = workspace.load_config(playbook, param)
    PrepareCmd().run(workspace, config)


@cli.command()
@click.option('--param', '-p', multiple=True, type=KeyValueParamType())
@click.argument('playbook', required=True)
@click.pass_obj
def run(workspace: Workspace, param: Optional[Tuple[str, str]], playbook: str):
    config = workspace.load_config(playbook, param)
    RunCmd().run(workspace, config)


@cli.command()
@click.option('--param', '-p', multiple=True, type=KeyValueParamType())
@click.argument('playbook', required=True)
@click.argument('executable', required=True, type=Path)
@click.argument('cli_args', nargs=-1, type=click.UNPROCESSED)
@click.pass_obj
def exec_local(workspace: Workspace, param: Optional[Tuple[str, str]], playbook: str, executable: Path,
               cli_args: Tuple[str]):
    config = workspace.load_config(playbook, param)
    ExecLocalCmd().run(workspace, config, executable, list(cli_args))


@cli.command("exec")
@click.option('--param', '-p', multiple=True, type=KeyValueParamType())
@click.argument('playbook', required=True)
@click.argument('executable', required=True, type=Path)
@click.argument('cli_args', nargs=-1, type=click.UNPROCESSED)
@click.pass_obj
def exec_remote(workspace: Workspace, param: Optional[Tuple[str, str]], playbook: str, executable: Path,
                cli_args: Tuple[str]):
    config = workspace.load_config(playbook, param)
    ExecRemoteCmd().run(config, executable, list(cli_args))


@cli.command()
@click.option('--group-by', '-g', type=str,
              help='Group multiple playbooks into a single node identified by this yaml property path')
@click.option('--root-by', '-r', type=str,
              help='Create an additional root node identified by this yaml property path')
@click.option('--link', '-l', type=str,
              help='Create an optional hyperlink on playbooks containing a yaml property of this path')
@click.argument('playbooks', required=True, nargs=-1)
@click.pass_obj
def graph(obj: Workspace, group_by: Optional[str], root_by: Optional[str], link: Optional[str], playbooks: List[str]):
    GraphCmd().run(playbooks, obj, group_by, root_by, link)


@cli.command()
def version():
    print('2.0-RC1')


def cli_entry():
    cli(auto_envvar_prefix='WELDER')


if __name__ == '__main__':
    cli_entry()
