import sys
from functools import reduce
from pathlib import Path
from typing import List, Dict, Optional

from graphviz import Digraph

from welder.config import Config
from welder.workspace import Workspace


class GraphCmd:

    def run(self, playbook_list: List[str], workspace: Workspace, group_by: Optional[str], root_by: Optional[str],
            link_attribute: Optional[str]):

        playbooks = dict(self.load_playbooks(playbook_list, workspace.base_dir))
        print(self.create_graph(playbooks, group_by, root_by, link_attribute))

    def load_playbooks(self, playbook_list: List[str], workdir: Path):
        for playbook in playbook_list:
            # sanitize accidental .yml file extension
            playbook_name = playbook[:-4] if playbook.endswith('.yml') else playbook
            yield playbook_name, Config(self.load_playbook(playbook_name, workdir))

    @staticmethod
    def load_playbook(playbook_name: str, workdir: Path) -> Dict:
        playbook_file = workdir / '{}.yml'.format(playbook_name)
        yaml = Workspace.load_yaml_file(playbook_file)

        if 'modules' in yaml:
            return yaml
        else:
            print('no "modules" section found in {}'.format(playbook_file), file=sys.stderr)
            sys.exit(1)

    @staticmethod
    def create_graph(playbooks: Dict[str, Config], group_by: Optional[str], root_by: Optional[str],
                     link_attribute: Optional[str]) -> str:

        dot = Digraph(comment='welder setting', graph_attr={'rankdir': 'LR'})

        for playbook in playbooks:
            playbook_config = playbooks[playbook]

            p_key = playbook_config.get(group_by, playbook)

            node_attributes = {}

            if playbook.endswith('test'):
                node_attributes['style'] = 'dotted'

            link_value = playbook_config.get(link_attribute)
            if link_value:
                node_attributes['target'] = '_blank'
                node_attributes['href'] = 'https://' + link_value
                node_attributes['fontcolor'] = '#2e39b3'

            dot.node(p_key, **node_attributes)

            root = playbook_config.get(root_by)
            if root:
                root_key = 'r_{}'.format(root)
                dot.node(root_key, root, shape='doublecircle')
                dot.edge(root_key, p_key)
            else:
                print("root-by property '{}' doesn't exist in playbook '{}'".format(root_by, playbook),
                      file=sys.stderr)
                sys.exit(1)

            for module in playbook_config.content['modules']:
                dot.node(module, shape='component')
                dot.edge(p_key, module)

        return dot.source
