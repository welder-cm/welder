import atexit
import getpass
from pathlib import Path

from welder.cli_helper import print_user, print_info, print_success, print_warn
from welder.config import Config
from welder.module import Module
from welder.remote_server import RemoteServer
from welder.ssh_connection import SshConnection
from welder.workspace import Workspace


class RunCmd:

    def __init__(self):
        self.is_completed = False

    def run(self, workspace: Workspace, config: Config):
        # Ask for sudo password on demand
        sudo_password = None
        if config.get('ask_sudo_password', False):
            print_user("Sudo password for {}:".format(config.get('ssh.url')))
            sudo_password = getpass.getpass("")

        ssh = SshConnection(config.get('ssh.url'), str(config.get('ssh.port')), sudo_password)

        atexit.register(self.exit_handler)
        print()

        with workspace.local_cache() as local_cache:
            for module_name in config.content.get('modules', []):
                print_info("module '{}'".format(module_name))

                module = Module(
                    config.content, local_cache.get_module_path(module_name), workspace.get_module_path(module_name))
                module.prepare()

                with RemoteServer(ssh) as server:
                    server.upload(local_cache.get_module_path(module_name))
                    server.execute_cmd(config.content, Path('setup.sh'))

                # add line break after possible final server output possibly without trailing line break
                print()
                print_success("module '{}'".format(module_name))

        self.is_completed = True

    def exit_handler(self):
        if self.is_completed:
            print_success("all done!")
        else:
            print("  …")
            print_warn("Execution aborted!")
