from welder import launcher
from welder.config import Config
from welder.workspace import Workspace


class ConfigCmd:
    """Compose the configuration values and print them on the console"""

    def run(self, config: Config):
        env_script = launcher.create_shell_var_commands(config.content)
        print(env_script)
