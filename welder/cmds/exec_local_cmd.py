from pathlib import Path
from typing import List, Optional, Dict

from welder.config import Config
from welder.workspace import Workspace


class ExecLocalCmd:
    """Execution of a local program providing the welder environment variables"""

    def run(self, workspace: Workspace, config: Config, local_script: Path, cli_args: List[str]):
        cmd = [str(local_script)] + cli_args

        workspace.run_executable(cmd, config.content, workspace.base_dir)
