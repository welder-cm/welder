from welder.config import Config
from welder.module import Module
from welder.workspace import Workspace


class PrepareCmd:

    def run(self, workspace: Workspace, config: Config):
        with workspace.local_cache(False) as local_cache:
            for module_name in config.content.get('modules', []):
                module = Module(
                    config.content, local_cache.get_module_path(module_name), workspace.get_module_path(module_name))
                module.prepare()

            print(local_cache.temp_path)
