import getpass
from pathlib import Path
from typing import List

from welder.cli_helper import print_user, print_success
from welder.config import Config
from welder.remote_server import RemoteServer
from welder.ssh_connection import SshConnection


class ExecRemoteCmd:
    """Execution of a local program on the remote side providing the welder environment variables"""

    def run(self, config: Config, local_script: Path, cli_args: List[str]):
        # Ask for sudo password on demand
        sudo_password = None
        if config.get('ask_sudo_password', False):
            print_user("Sudo password for {}:".format(config.get('ssh.url')))
            sudo_password = getpass.getpass("")

        ssh = SshConnection(config.get('ssh.url'), str(config.get('ssh.port')), sudo_password)

        with RemoteServer(ssh) as server:
            server.upload(local_script)
            server.execute_cmd(config.content, str(local_script.name), cli_args)
