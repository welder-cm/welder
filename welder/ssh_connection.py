import shlex
import subprocess
from pathlib import Path
from typing import List

import pexpect


class SshConnection:

    def __init__(self, ssh_url: str, ssh_port: str, sudo_password: str = None):
        self.ssh_url = ssh_url
        self.ssh_port = ssh_port
        self.sudo_password = sudo_password

        self.remote_cmd_prefix = ['ssh', '-qp', self.ssh_port, self.ssh_url, '--']

    def exec(self, remote_command: List[str], check: bool = False):
        ssh_cmd = self.remote_cmd_prefix + [shlex.quote(str(component)) for component in remote_command]
        subprocess.run(ssh_cmd, check=check)

    def exec_script(self, remote_command: str, check: bool = False):
        ssh_cmd = ['ssh', '-qtp', self.ssh_port, self.ssh_url, '--', remote_command]

        if self.sudo_password is None:
            subprocess.run(ssh_cmd, check=check)
        else:
            child = pexpect.spawn(ssh_cmd[0], ssh_cmd[1:])
            index = child.expect(['\[sudo\] password for .*:', pexpect.EOF])
            if index == 0:
                child.sendline(self.sudo_password)
            elif index == 1:
                # pass-through the error message
                print(child.before.decode('utf-8'))

            child.interact()  # Give control of the child to the user.
            child.close()

            return_code = child.exitstatus
            if return_code != 0:
                raise subprocess.CalledProcessError(return_code, ssh_cmd)

    def upload(self, source_path: Path, remote_path: Path):
        # upload the content of folders or else only the file itself
        source_path_value = '{}/'.format(source_path) if source_path.is_dir() else str(source_path)

        # rsync compiled files to the server, skipping templates (liquid, j2)
        upload_cmd = ['rsync', '--archive', '--delete', '--quiet', '--prune-empty-dirs',
                      '--rsh', 'ssh -p {}'.format(self.ssh_port),
                      '--exclude', '*.j2',
                      source_path_value, '{}:{}/'.format(self.ssh_url, remote_path)]

        subprocess.run(upload_cmd, check=True)
